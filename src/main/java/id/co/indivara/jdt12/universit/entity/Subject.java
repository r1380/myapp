package id.co.indivara.jdt12.universit.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subject")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Subject extends BaseEntity {
    @Id
    @Column(name = "lecture_id")
    private Long lectureId;
    @Column(name = "studentId")
    private Long studentId;
    @Column(name = "nama")
    private String nama;
    @Column(name = "deskripsi")
    private String deskripsi;

}
