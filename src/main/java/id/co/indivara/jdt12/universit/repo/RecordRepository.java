package id.co.indivara.jdt12.universit.repo;

import id.co.indivara.jdt12.universit.entity.Record;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordRepository extends JpaRepository<Record,Long> {
}
