package id.co.indivara.jdt12.universit.controller;

import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.repo.ResponMessage;
import id.co.indivara.jdt12.universit.repo.StudentRepository;
import id.co.indivara.jdt12.universit.service.StudentService;
import id.co.indivara.jdt12.universit.service.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class StudentController {
    @Autowired
    StudentRepository studentRepository;


    @PostMapping("/add")
    public ResponMessage addStudent(@RequestBody Student student){
        studentRepository.save(student);
        return new ResponMessage("Data successfully entered." +student+" ",200);

    }
    @DeleteMapping("/delete/{studentId}")
    public ResponMessage deleteStudent(@PathVariable("studentId") Long studentId) {
        studentRepository.deleteById(studentId);
        return new ResponMessage("Data successfully deleted.",200);
    }

    //findbystudentId
    @GetMapping("/find/{studentId}")
    public Student findStudent(@PathVariable Long studentId){
        Student student = studentRepository.findById(studentId).orElseThrow(() -> new RuntimeException("Student not found"));
        return student;
    }
    //lihat semua student
    @GetMapping("/all")
    public ArrayList<Student> findAllStudent(){
        ArrayList<Student> student=(ArrayList<Student>) studentRepository.findAll();
        return student;
    }
    //findname
    @GetMapping("/findname")
    public ArrayList<Student> findUserByName(@RequestParam ("nama")String name) {
        ArrayList<Student> student = (ArrayList<Student>) studentRepository.findByNama(name);
        return student;
    }
    @PutMapping("/student/{studentId}")
    public Student updateStudent (@RequestBody Student student, @PathVariable("studentId") Long studentId) throws InstantiationException, IllegalAccessException {

        return StudentService.class.newInstance().saveDepartment(student);

    }

    //update
//    @PutMapping("/update{studentId}")
//    public ResponseEntity<String> updateStudent(@RequestBody Student updatedStudent) {
//        try {
//            Student student = studentRepository.findById(updatedStudent.getStudentId()).orElseThrow(() -> new RuntimeException("Student not found"));
//            student.setStudentId(updatedStudent.getStudentId());
//            student.setNama(updatedStudent.getNama());
//            student.setTanggal(updatedStudent.getTanggal());
//            student.setAlamat(updatedStudent.getAlamat());
//            student.setNomor(updatedStudent.getNomor());
//            student.setEmail(updatedStudent.getEmail());
//            student.setSubject(updatedStudent.getSubject());
//
//            // Update property lain sesuai kebutuhan
//            studentRepository.save(student);
//            return ResponseEntity.ok("Student updated successfully.");
//        } catch (RuntimeException e) {
//            return ResponseEntity.notFound().build();
//            }
//        }


    }
