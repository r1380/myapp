package id.co.indivara.jdt12.universit.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "record")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Record extends BaseEntity{
    @Id
    @Column(name = "student_id")
    private Long studentId;
    @Column(name = "lecture_id")
    private Long lectureId;
    @Column(name = "quis")
    private float quis;
    @Column(name = "mid_test")
    private float midTest;
    @Column(name = "final_grade")
    private String finalGrade;
    @Column(name = "periode")
    private String periode;
}
