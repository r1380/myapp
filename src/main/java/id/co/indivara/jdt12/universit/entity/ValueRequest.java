package id.co.indivara.jdt12.universit.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name = "record")
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
//@Builder
public class ValueRequest extends BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long studentId;
    @Column(name = "quis")
    private int quiz;
    @Column(name = "mid_test")
    private int midTest;
    @Column(name = "final_test")
    private int finaltest;
    @Column(name = "grade")
    private int grade;

}
