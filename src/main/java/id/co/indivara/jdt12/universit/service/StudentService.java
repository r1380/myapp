package id.co.indivara.jdt12.universit.service;

import id.co.indivara.jdt12.universit.entity.Student;
import id.co.indivara.jdt12.universit.repo.StudentRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public interface StudentService  {

    public void save(Student student);


    //POST("/department")
    Student saveDepartment(Student department);
    //GET : /department
    List<Student> studentList();
    //PUT : /student/{id}
    Student updateStudent (Student student, Long studentId);

    //Delete : /student/{id}
    void deleteStudent(Long studentId);
}
